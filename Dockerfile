FROM debian:buster
RUN apt-get update \
  && apt install -y syslinux pxelinux dnsmasq wget xorriso
RUN mkdir /tftp
WORKDIR /tftp
RUN cp -v /usr/lib/PXELINUX/pxelinux.0 /tftp/
RUN ["/bin/bash", "-c", "cp -v /usr/lib/syslinux/modules/bios/{ldlinux.c32,libcom32.c32,libutil.c32,vesamenu.c32} /tftp/"]
RUN mkdir pxelinux.cfg
COPY ./Parrot-netinstall-4.9.1_x64.iso .
RUN xorriso -osirrox on -indev Parrot-netinstall-4.9.1_x64.iso -extract / /tmp \
  && cp -v /tmp/linux /tmp/initrd.gz .
COPY ./default ./pxelinux.cfg/
CMD ip a &&\
    dnsmasq --interface=enp0s20f0u2 \
      --dhcp-broadcast \
      --dhcp-range=10.10.10.101,10.10.10.199,255.255.255.0,1h \
      --dhcp-boot=pxelinux.0,pxeserver,10.10.10.1 \
      --pxe-service=x86PC,"Install Linux",pxelinux \
      --enable-tftp --tftp-root=/tftp/ --no-daemon
